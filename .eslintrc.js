module.exports = {
  env: {
    browser: true,
    es2020: true,
  },
  extends: ['plugin:react/recommended', 'plugin:react-hooks/recommended', 'airbnb', 'airbnb/hooks', 'eslint:recommended'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      js: true,
    },
    ecmaVersion: 2020,
    sourceType: 'module',
    requireConfigFile: false,
    babelOptions: {
      presets: ['@babel/preset-react']
    },
  },
  plugins: ['react', 'react-hooks'],
  parser: '@babel/eslint-parser',
  rules: {
    'no-shadow': 0,
    'react-hooks/exhaustive-deps': 0,
    'object-curly-newline': ['error', {
      ObjectExpression: 'always',
      ObjectPattern: {
        multiline: true
      },
      ImportDeclaration: {
        multiline: true
      },
      ExportDeclaration: {
        multiline: true
      }
    }],
    'no-unused-vars': 1,
    'react/jsx-no-constructed-context-values': 0,
    'react/react-in-jsx-scope': 'off',
    'react/prop-types': 0,
    'linebreak-style': ['error', 'windows'],
    'comma-dangle': [
      'error',
      {
        functions: 'never',
        objects: 'only-multiline',
      }
    ],
    'import/prefer-default-export': 0,
    'react/state-in-constructor': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/no-noninteractive-element-interactions': 0,
    'arrow-body-style': 0,
    'react/jsx-filename-extension': [1, {
      extensions: ['.js', '.jsx']
    }],
    quotes: [
      'error',
      'single',
      {
        allowTemplateLiterals: true
      }
    ],
    'react/function-component-definition': [
      2,
      {
        namedComponents: 'function-declaration'
      }
    ],
  },
};
