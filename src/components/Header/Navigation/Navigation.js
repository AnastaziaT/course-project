import React from 'react';
import { NavLink } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import classNames from 'classnames';
import { auth } from '../../../services/firebase';
import styles from './Navigation.module.css';
import { routes } from '../../App/constants';

export default function Navigation() {
  const [authUser] = useAuthState(auth);

  return (
    <div className="m-2">
      <ul className="nav justify-content-end">
        <li className="nav-item">
          <NavLink
            className={classNames('nav-link-light', styles.nav)}
            to={routes.home}
          >
            Главная
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            className={classNames('nav-link-light', styles.nav)}
            to={routes.aboutProject}
          >
            О проекте
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            className={classNames('nav-link-light', styles.nav)}
            to={routes.map}
          >
            Как нас найти
          </NavLink>
        </li>
        {authUser
          ? (
            <>
              <li className="nav-item">
                <NavLink
                  onClick={() => auth.signOut()}
                  className={classNames('nav-link-light', styles.nav)}
                  to={routes.home}
                >
                  <i className="fa fa-male" />
                  Выйти
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  className={classNames('nav-link-light', styles.nav)}
                  to={routes.personalArea}
                >
                  <i className="fa fa-suitcase" />
                  Личный кабинет
                </NavLink>
              </li>
            </>
          )
          : (
            <li className="nav-item">
              <NavLink
                className={classNames('nav-link-light', styles.nav)}
                to={routes.startAutorization}
              >
                <i className="fa fa-male" />
                Войти
              </NavLink>
            </li>
          )}

      </ul>
    </div>
  );
}
