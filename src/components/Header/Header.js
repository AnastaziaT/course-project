import React from 'react';
import Navigation from './Navigation/Navigation';

export default function Header() {
  return (
    <div>
      <div className="card text-white bg-primary text-left">
        <div className="card-body">
          <h5 className="card-title">Link Shortener</h5>
        </div>
        <Navigation />
      </div>
    </div>
  );
}
