import React from 'react';
import styles from './Map.module.css';

export default function Map() {
  return (
    <div className={styles.map}>
      <div className="jumbotron">
        <h1 className="display-6"><b>Как нас найти?</b></h1>
        <p className="lead">Адрес: город Москва, Манежная площадь, строение 2</p>
        <hr className="my-4" />
      </div>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2245.1575840601854!2d37.613085215260384!3d55.755764699231946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54a507638da87%3A0x8995ebed3956cf8!2z0JzQsNC90LXQttC90LDRjyDQv9C7Liwg0YHRgtGA0L7QtdC90LjQtSAyLCDQnNC-0YHQutCy0LAsIDEyNTAwOQ!5e0!3m2!1sru!2sru!4v1638282099209!5m2!1sru!2sru"
        width="600"
        height="450"
        allowFullScreen=""
        loading="lazy"
        title="map"
      />
    </div>
  );
}
