import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { useAuthState } from 'react-firebase-hooks/auth';
import { signIn, auth } from '../../../services/firebase';
import Greeting from './Greeting/Greeting';
import styles from './StartAutorization.module.css';
import { routes } from '../../App/constants';

export default function StartAutorization() {
  const [authState] = useAuthState(auth);

  const [isModalShow, setIsModalShow] = useState(false);

  useEffect(() => {
    if (authState) {
      setIsModalShow(true);
    }
  }, [authState]);

  const handleClickCloseModal = () => {
    setIsModalShow(false);
  };

  return (
    <div className={styles.startAuto}>
      <div className="jumbotron">
        <h1 className="display-6"><b>Авторизуйтесь,</b></h1>
        <p className="lead">чтобы отслеживать историю своих запросов</p>
        <hr className="my-4" />
        <p className="lead">
          <Button onClick={signIn}>Войти в личный кабинет</Button>
        </p>
      </div>
      {authState && (
        <NavLink to={routes.personalArea}>
          <Greeting
            isModalShow={isModalShow}
            handleClickCloseModal={handleClickCloseModal}
          />
        </NavLink>
      )}
    </div>
  );
}
