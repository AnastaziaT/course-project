import React from 'react';
import { Modal } from 'react-bootstrap';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from '../../../../services/firebase';

export default function Greeting({ isModalShow, handleClickCloseModal }) {
  const [authState] = useAuthState(auth);

  if (!authState) {
    return null;
  }

  return (
    <Modal show={isModalShow} onHide={handleClickCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Вход выполнен успешно</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {`Добро пожаловать, ${authState.displayName}!`}
      </Modal.Body>
      <Modal.Footer />
    </Modal>
  );
}
