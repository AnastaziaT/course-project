import React from 'react';
import styles from './Home.module.css';
import FreeStart from './FreeStart/Freestart';
import Picture from './Picture/Picture';

export default function Home() {
  return (
    <div className="d-flex mx-auto">
      <div className={styles.freeStart}>
        <FreeStart />
      </div>
      <div>
        <Picture />
      </div>
    </div>
  );
}
