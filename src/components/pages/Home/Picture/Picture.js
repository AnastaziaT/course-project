import React from 'react';
import styles from './Picture.module.css';

export default function Picture() {
  return (
    <img className={styles.picture} src="img/digitization-6399683.jpg" alt="logo" />
  );
}
