import React from 'react';
import { Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { routes } from '../../../App/constants';

export default function FreeStart() {
  return (
    <div className="jumbotron">
      <h1 className="display-4">Инструмент коротких URL адресов</h1>
      <p className="lead">Простой метод укоротить ссылку с массой преимуществ!</p>
      <hr className="my-4" />
      <p>С помощью нашего сервиса вы сможете бесплатно сделать короткую ссылку из длинной.</p>
      <p className="lead">
        <NavLink to={routes.linkShortener}>
          <Button size="lg">Начать бесплатно</Button>
        </NavLink>
      </p>
    </div>
  );
}
