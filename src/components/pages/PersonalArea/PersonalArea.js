import { useEffect, useState } from 'react';
import { usePageAccess } from '../../../utils/usePageAccess';
import { useLinksByAuthUser } from '../../../modules/links/links';
import ButtonForShortLinks from '../../ButtonForShortLinks/ButtonForShortLinks';
import PendingButton from '../../PendingButton/PendingButton';

export default function PersonalArea() {
  const [authState] = usePageAccess();
  const { links, getLinksHistory } = useLinksByAuthUser(authState);
  const [isPending, setIsPending] = useState(true);

  useEffect(() => {
    (async () => {
      if (authState) {
        await getLinksHistory();
        setIsPending(false);
      }
    })();
  }, [authState]);

  if (!authState) {
    return null;
  }

  return (
    <div className="m-5 p-3">
      <div className="text-center">
        <h1 className="display-6"><b>История запросов</b></h1>
        <p className="lead"> Вы всегда можете посмотреть историю своих запросов в личном кабинете.</p>
        <hr className="my-4" />
      </div>
      {
        isPending
          ? (
            <div
              className="mx-auto"
              style={{
                width: '200px'
              }}
            >
              <PendingButton />
            </div>
          )
          : (
            links.map(({ link, shortLink }) => {
              return (
                <div key={link} className="m-3 p-3 bg-primary text-light rounded">
                  <div>
                    Исходная ссылка:
                    {' '}
                    {link}
                  </div>
                  <div>
                    Короткая ссылка:
                    {' '}
                    {shortLink}
                  </div>
                  <ButtonForShortLinks shortLink={shortLink} />
                </div>
              );
            })
          )
      }
      { !isPending && links.length === 0 && <div>В настоящий момент ваша история пуста :(</div>}
    </div>
  );
}
