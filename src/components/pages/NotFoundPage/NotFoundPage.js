import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import styles from './NotFoundPage.module.css';
import { routes } from '../../App/constants';

export default function NotFoundPage() {
  return (
    <div className={styles.notFoundPage}>
      <div className="jumbotron">
        <h1 className="display-6"><b>Упс!</b></h1>
        <p className="lead">Кажется, что-то пошло не так</p>
        <hr className="my-4" />
        <p className="lead">
          <NavLink to={routes.home}>
            <Button size="lg">Вернуться на главную</Button>
          </NavLink>
        </p>
      </div>
    </div>
  );
}
