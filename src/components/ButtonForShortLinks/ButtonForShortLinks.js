import React, { useState } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import QRCode from 'qrcode.react';
import classNames from 'classnames';
import styles from './ButtonForShortLinks.module.css';
import downloadQRCode from '../../services/downloadQRCode';

export default function ButtonForShortLinks({ shortLink }) {
  const [isCopySuccess, setCopySuccess] = useState(false);
  const [qrCode, setQrCode] = useState(false);

  const handleClickCopy = () => {
    navigator.clipboard.writeText(shortLink);
    setCopySuccess(true);
    setTimeout(() => setCopySuccess(false), 300);
  };

  const handleClickQrCode = () => {
    setQrCode(!qrCode);
  };

  const handleClickDownloadQRCode = () => {
    downloadQRCode(shortLink);
  };

  return (
    <div>
      {
        isCopySuccess
        && (
          <button type="button" className={classNames('btn btn-light btn-sm', styles.note)} data-toggle="tooltip" data-placement="bottom" title="Скопировано">
            Скопировано
          </button>
        )
      }
      <ButtonGroup aria-label="Basic example" className={styles.button}>
        <Button
          onClick={handleClickCopy}
          data-toggle="tooltip"
          data-placement="bottom"
          title="Копировать"
        >
          <i className="fa fa-copy" />
        </Button>
        <Button
          onClick={handleClickQrCode}
          data-toggle="tooltip"
          data-placement="bottom"
          title="Сгенерировать QR-код"
        >
          <i className="fa fa-qrcode" />
        </Button>
      </ButtonGroup>
      {
        qrCode
        && (
        <div className="text-center">
          <div className={styles.qr}>
            <QRCode
              id="qr-gen"
              value={shortLink}
              size={290}
              level="H"
              includeMargin
            />
          </div>
          <Button
            onClick={handleClickDownloadQRCode}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Скачать QR-код"
          >
            <i className="fa fa-download" />
          </Button>
        </div>
        )
      }
    </div>
  );
}
