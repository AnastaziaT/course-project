import React from 'react';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false
    };
  }

  static getDerivedStateFromError() {
    return {
      hasError: true
    };
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;

    if (hasError) {
      return <NotFoundPage />;
    }

    return children;
  }
}
