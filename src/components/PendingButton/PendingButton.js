import { Button, Spinner } from 'react-bootstrap';

export default function PendingButton() {
  return (
    <Button className="m-3" disabled>
      <Spinner
        as="span"
        animation="border"
        size="sm"
        role="status"
        aria-hidden="true"
      />
      Загрузка...
    </Button>
  );
}
