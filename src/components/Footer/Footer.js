import styles from './Footer.module.css';

export default function Footer() {
  return (
    <div className="display-1 text-white bg-primary  text-center">
      <div className={styles.contacts}>
        <div className={styles.name}>
          <b>2021 © AnastaziaT</b>
          <div className={styles.rights}>
            Все права защищены
          </div>
        </div>
        <div className="card-text">
          <p><b>Где мы находимся:</b></p>
          <div>
            <div>
              г. Москва,
            </div>
            Манежная пл., стр. 2
          </div>
        </div>
        <div className="card-text">
          <p><b>Связаться с нами:</b></p>
          <div>
            <a href="tel:+79999999999">
              <i className="fa fa-phone-square" />
              +7(999)-999-99-99
            </a>
          </div>
          <div>
            <a href="mailto:fakemail@mail.ru">
              <i className="fa fa-envelope" />
              fakemail@mail.ru
            </a>
          </div>
        </div>
      </div>

    </div>
  );
}
