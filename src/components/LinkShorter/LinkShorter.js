import React, { useState } from 'react';
import { Button } from 'react-bootstrap';

import { useAuthState } from 'react-firebase-hooks/auth';
import LinkInput from './LinkInput/LinkInput';
import styles from './LinkShorter.module.css';
import { getShortLink } from '../../services/getShortLink';
import inputValidation from '../../services/inputValidation';
import { useLinksByAuthUser } from '../../modules/links/links';
import ButtonForShortLinks from '../ButtonForShortLinks/ButtonForShortLinks';
import PendingButton from '../PendingButton/PendingButton';
import { auth } from '../../services/firebase';

export default function LinkShorter() {
  const [isPending, setIsPending] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [shortLink, setShortLink] = useState('');
  const [inputError, setErrorMessage] = useState(false);
  const [authState] = useAuthState(auth);

  const { addNewLink, getLinksHistory } = useLinksByAuthUser(authState);

  const handleClickSubmit = async () => {
    if (!inputValue || !inputValidation(inputValue)) {
      setErrorMessage(true);
      return;
    }
    setIsPending(true);
    try {
      if (!authState) {
        const result = await getShortLink(inputValue);

        setShortLink(result.shortLink);
      } else {
        const links = await getLinksHistory();
        const foundLinkObject = links.find(({ link }) => link === inputValue);

        let shortLink = '';
        if (foundLinkObject) {
          shortLink = foundLinkObject.shortLink;
        } else {
          const result = await getShortLink(inputValue);
          shortLink = result.shortLink;
          await addNewLink(inputValue, result.shortLink);
        }

        setShortLink(shortLink);
      }
      setIsPending(false);
      setErrorMessage(false);
    } catch (error) {
      console.error(error);
      setIsPending(false);
    }
  };

  const handleChangeInput = (event) => {
    setInputValue(event.target.value.trim());
  };

  return (
    <div className={styles.LinkShorter}>
      {
        inputError
          && (
          <div className={styles.mesage}>
            *Формат ссылки должен быть таким: &quot;https://your-link.com&quot;!
          </div>
          )
      }
      <LinkInput onChange={handleChangeInput} link={inputValue} />
      {
        isPending
          ? (
            <PendingButton />
          )
          : (
            <div>
              <Button
                className={styles.button}
                onClick={handleClickSubmit}
                data-toggle="tooltip"
                data-placement="bottom"
                title="Сгенерировать короткую ссылку"
              >
                Сократить
              </Button>
            </div>
          )
      }
      {shortLink && (
        <div>
          <div className="form-control" role="alert">
            {shortLink}
          </div>
          <ButtonForShortLinks shortLink={shortLink} />
        </div>
      )}
    </div>
  );
}
