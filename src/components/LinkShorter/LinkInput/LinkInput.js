import React from 'react';

export default function LinkInput({ link, onChange }) {
  return (
    <input
      name="link"
      value={link}
      placeholder="Вставьте ссылку для сокращения"
      className="form-control"
      onChange={onChange}
    />
  );
}
