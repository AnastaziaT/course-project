import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';

import LinkShorter from '../LinkShorter/LinkShorter';
import Header from '../Header/Header';
import Home from '../pages/Home/Home';
import Footer from '../Footer/Footer';
import StartAutorization from '../pages/StartAutorization/StartAutorization';
import PersonalArea from '../pages/PersonalArea/PersonalArea';
import AboutProject from '../pages/AboutProject/AboutProject';
import NotFoundPage from '../pages/NotFoundPage/NotFoundPage';
import Map from '../pages/Map/Map';
import { routes } from './constants';
import ErrorBoundary from '../ErrorBoundary';

export default function App() {
  return (
    <ErrorBoundary>
      <Router>
        <Header />
        <main>
          <Routes>
            <Route path="*" element={<NotFoundPage />} />
            <Route path="/" element={<Home />} />
            <Route path={routes.aboutProject} element={<AboutProject />} />
            <Route path={routes.linkShortener} element={<LinkShorter />} />
            <Route path={routes.map} element={<Map />} />
            <Route path={routes.startAutorization} element={<StartAutorization />} />
            <Route path={routes.personalArea} element={<PersonalArea />} />
          </Routes>
        </main>
        <Footer />
      </Router>
    </ErrorBoundary>
  );
}
