export const routes = {
  linkShortener: '/link-shortener',
  aboutProject: '/about-project',
  map: '/map',
  startAutorization: '/start-autorization',
  home: '/',
  personalArea: '/personal-area'
};
