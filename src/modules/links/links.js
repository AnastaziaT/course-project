import { useState } from 'react';
import { collection, getDocs, addDoc, query, where } from 'firebase/firestore';

import { db } from '../../services/firebase';

export const linksCollectionName = 'links';

export function useLinksByAuthUser(authState) {
  const [links, setLinks] = useState([]);

  const linksCollectionRef = collection(db, linksCollectionName);

  const addNewLink = async (link, shortLink) => {
    if (!authState) {
      return;
    }

    await addDoc(linksCollectionRef, {
      userId: authState.uid,
      link,
      shortLink,
    });
  };

  const getLinksHistory = async () => {
    if (!authState) {
      return null;
    }

    const getDocsData = await getDocs(query(linksCollectionRef, where('userId', '==', authState.uid)));

    const links = getDocsData.docs.map((doc) => ({
      ...doc.data(), id: doc.id
    }));

    setLinks(links);

    return links;
  };

  return {
    links,
    addNewLink,
    getLinksHistory,
  };
}
