import { useAuthState } from 'react-firebase-hooks/auth';
import { useNavigate } from 'react-router-dom';
import { auth } from '../services/firebase';
import { routes } from '../components/App/constants';

export function usePageAccess() {
  const [authState] = useAuthState(auth);

  const navigate = useNavigate();

  if (!authState) {
    navigate(routes.home);
  }

  return [authState];
}
