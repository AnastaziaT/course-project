export default function inputValidation(input) {
  const regExp = /https?:\/\/\S+/;
  if (regExp.test(input)) {
    return true;
  }
  return false;
}
