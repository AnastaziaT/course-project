export const getShortLink = async (inputLink) => {
  try {
    const response = await fetch(
      `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${process.env.REACT_APP_FB_API_KEY}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          dynamicLinkInfo: {
            domainUriPrefix: 'https://anastazia.page.link',
            link: inputLink,
          }
        })
      }
    );

    const result = await response.json();
    return result;
  } catch (error) {
    return `${error}`;
  }
};
